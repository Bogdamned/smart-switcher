//Прошивка для ардуино: включает/выклювает реле по двойному хлопку.

const int Relay = 4;        //Подключаем реле на 4-й цифровой выход
const int soundSensor = 3;  //Подключаемзвуковой датчик на 3-й аналаговый вход

int Volume = 0;
boolean   RelayState = 0;   

void setup() {
  pinMode(Relay, OUTPUT);
}
  
void loop() {
  Volume = analogRead(soundSensor); 
  if(Volume > 500 && Volume < 600)
  {
    delay(175); 
    for(int t=0; t<=500; t++)
    {
      delay(1);
      Volume = analogRead(soundSensor); 
      if(Volume > 500 && Volume < 600)
      {
        RelayState = !RelayState;
        digitalWrite(Relay,RelayState);
        break;
      }
    }
  }
}

boolean Relay=0;            //Переменная для хранения состояния реле
boolean isExecuting = false;
int cmd = 0;            //код выполняемой команды


void setup() {
  pinMode(3, OUTPUT); //переводим пин 3 в  режим выхода
  Serial.begin(9600); //инициализация COM-порта (9600 - скорость обмена)
}
  
void loop() {

  if (Serial.available() > 0) //если на COM-порт пришла какая-то информация
    {
      if (isExecuting == false) //если в данный момент не идет выполнение никакой команды
        {
          cmd = Serial.read()-'0'; //считываем код выполняемой команды
          isExecuting = true; //теперь переменная показывает, что началось выполнение команды
        }
      
      if (cmd == 1) //управление реле
       {
         swRelay()
         isExecuting = false;
       }
       //Включаем режим стробоскопа :)
       if(cmd == 2)
       {
          strobMode();
          isExecuting = false;
       }

       isExecuting = false;
    }
  
}

void swRelay(){
  Relay=!Relay;
  digitalWrite(3,Relay);
}
void strobMode(){
  while (true){
    Relay=!Relay;
    digitalWrite(3,Relay);

    if (Serial.available() > 0) //если на COM-порт пришла какая-то информация
    {
      cmd = Serial.read()-'0'; //считываем код выполняемой команды
      if(cmd == 2)
      {
        break;
      }
    }
    delay(75);
  }
  
}

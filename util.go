package main

import (
	"gopkg.in/telegram-bot-api.v4"
)

func canAccess(update *tgbotapi.Update) bool {
	canAccess := false
	var uid int

	if update.Message != nil {
		uid = update.Message.From.ID
	} else {
		uid = update.CallbackQuery.From.ID
	}

	if uid == bot.Root {
		bot.isRoot = true
		return true
	} else {
		bot.isRoot = false
	}

	for _, v := range bot.Rights {
		if uid == v {
			canAccess = true
			break
		}
	}

	return canAccess
}

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/tarm/serial"
)

type (
	//COMconnector is an instance for COM port connection
	COMconnector struct {
		Enabled   bool   `json:"enabled"`
		PortID    string `json:"portId"`
		Config    *serial.Config
		Port      *serial.Port
		connError error
	}
)

var (
	strobOn      = false
	comConnector = COMconnector{}
)

func (c *COMconnector) initSettings() {
	jsonFile, err := os.Open("json/COMsettings.json")
	if err != nil {
		log.Println("Unable to open COMSettings.json: ", err)
	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &c)

	if c.Enabled {
		c.Config = &serial.Config{Name: c.PortID, Baud: 9600}
		c.Port, c.connError = serial.OpenPort(c.Config)

		if c.connError != nil {
			c.Enabled = false
			log.Println("Failed to initialize connection to port: " + c.PortID + ", Error: \"" + c.connError.Error() + "\"")
		} else {
			log.Println("Initialized connection on port: " + c.PortID)
		}

	} else {
		comConnector = COMconnector{}
		log.Println("COM communication disabled.")
	}

}

func switcher() bool {
	return sendSignal("1")
}

func strobmode() bool {
	ret := false
	if sendSignal("2") {
		strobOn = !strobOn
		ret = true
	}
	return ret
}

func sendSignal(msg string) bool {
	ret := true

	if comConnector.Enabled && comConnector.Port != nil {
		n, err := comConnector.Port.Write([]byte(msg))
		if err != nil {
			log.Println(err, n)
			ret = false
		}

	} else {
		log.Println("Access to port:" + comConnector.PortID + " denied.")
		ret = false
	}

	return ret
}

func getSensorData() string {
	indicators := "T: 25C \nH: 50%"
	return indicators
}

func portSwitch() string {
	response := ""
	comConnector.Enabled = !comConnector.Enabled

	if comConnector.Enabled {
		comConnector.initSettings()

		if comConnector.Port == nil {
			response = fmt.Sprintf("Failed to establish connection. Connect device to " + comConnector.PortID)
		} else {
			response = fmt.Sprintf("COM connection enabled on " + comConnector.PortID)
		}

	} else {
		comConnector.Port.Close()
		comConnector = COMconnector{}
		response = fmt.Sprintf("COM connection disabled")
	}

	log.Println(response)

	return response
}

//BOT token: 603582668:AAG_iV65uI4PPED2Fe4xSjyV11NzPr6rb1k
package main

import (
	"fmt"
	"log"

	"gopkg.in/telegram-bot-api.v4"
)

const hardDisconnect string = "Oops... connect hard drive"

//Bot is custom bot instance
type Bot struct {
	botAPI *tgbotapi.BotAPI
	Token  string `json:"token" bson:"token"`
	Rights []int  `json:"rights" bson:"rights"`
	Root   int    `json:"root" bson:"root"`
	NoMsg  bool   `json:"nomsg" bson:"nomsg"`
	isRoot bool
}

var (
	bot Bot
)

func init() {
	bot.initBot()
	comConnector.initSettings()
}

func (bot *Bot) initBot() {
	err := BotConfig.FindOne(ctx, nil).Decode(&bot)

	if err != nil {
		log.Println("Unable to retreive tlg bot settings")
	} else {
		log.Println("Done reading bot settings.")
	}

	bot.botAPI, _ = tgbotapi.NewBotAPI(bot.Token)
	bot.botAPI.Debug = false

	log.Printf("Authorized on account %s", bot.botAPI.Self.UserName)
}

func main() {
	defer comConnector.Port.Close()
	runBot()
}

func runBot() {
	config := tgbotapi.NewUpdate(0)
	config.Timeout = 60
	updates, err := bot.botAPI.GetUpdatesChan(config)
	if err != nil {
		log.Println("Unable to get updates: ", err)
	}

	for {
		select {
		case update := <-updates:

			if canAccess(&update) {
				if update.Message != nil {
					if bot.NoMsg {
						noMsg(update.Message.Chat.ID)
					} else {
						processMsg(update.Message.Chat.ID, update.Message.Text)
					}
				} else {
					processCallback(update.CallbackQuery.Data, update.CallbackQuery.Message.Chat.ID)
				}
			}

		}

	}
}

func processCallback(request string, ChatID int64) {
	msg := tgbotapi.NewMessage(ChatID, "")

	switch request {
	case "switchBtn":
		msg.Text = "switched"
	case "strobBtn":
		msg.Text = fmt.Sprintf("Strobmode active: %v", strobOn)
	case "sensorDataBtn":
		msg.Text = getSensorData()
	case "comSwitchBtn":
		msg.Text = portSwitch()
	case "noMsgBtn":
		msg.Text = noMsgSwitch()
	default:
		msg.Text = "Use /switch or /strobe commands"
	}

	msg.ReplyMarkup = keyboard()
	bot.botAPI.Send(msg)
}

func noMsgSwitch() string {
	response := ""
	bot.NoMsg = !bot.NoMsg

	if bot.NoMsg {
		response = "NoMsg mode is on"
	} else {
		response = "NoMsg mode is off"
	}
	return response
}

func noMsg(ChatID int64) {
	msg := tgbotapi.NewMessage(ChatID, "")
	msg.ReplyMarkup = keyboard()
	msg.Text = "Messaging disabled, use buttons to control the bot."
	bot.botAPI.Send(msg)
}

func processMsg(ChatID int64, Text string) {
	msg := tgbotapi.NewMessage(ChatID, "")

	switch Text {
	case "/switch":
		ok := switcher()

		if ok {
			msg.Text = "switched"
		} else {
			msg.Text = hardDisconnect
		}
	case "/strobe":
		msg.Text = fmt.Sprintf("Strobmode active: %v", strobOn)

		ok := strobmode()
		if ok {
			msg.Text = fmt.Sprintf("Strobmode active: %v", strobOn)
		} else {
			msg.Text = hardDisconnect
		}

	default:
		msg.Text = "Use /switch or /strobe commands"
	}

	msg.ReplyMarkup = keyboard()
	bot.botAPI.Send(msg)
}

package main

import "gopkg.in/telegram-bot-api.v4"

var (
	//MiniKeyboard layout
	MiniKeyboard = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Показания", "sensorDataBtn"),
		),
	)

	//DefaultKeyboard layout
	DefaultKeyboard = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Показания", "sensorDataBtn"),
		),

		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Switch", "switchBtn"),
			tgbotapi.NewInlineKeyboardButtonData("Strobmode", "strobBtn"),
		),
	)
)

func keyboard() tgbotapi.InlineKeyboardMarkup {
	var keyboard tgbotapi.InlineKeyboardMarkup

	if comConnector.Enabled {
		keyboard = DefaultKeyboard
	} else {
		keyboard = MiniKeyboard
	}

	if bot.isRoot {
		keyboard = appendRootPanel(keyboard)
	}

	return keyboard
}

func appendRootPanel(keyboard tgbotapi.InlineKeyboardMarkup) tgbotapi.InlineKeyboardMarkup {
	tmp := make([][]tgbotapi.InlineKeyboardButton, len(keyboard.InlineKeyboard))
	tmp = keyboard.InlineKeyboard

	tmp = append(tmp, tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Вкл/Выкл COM port", "comSwitchBtn"),
	))

	tmp = append(tmp, tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Вкл/Выкл noMsg mode", "noMsgBtn"),
	))

	keyboard.InlineKeyboard = tmp

	return keyboard
}

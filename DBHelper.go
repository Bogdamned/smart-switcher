package main

import (
	"context"
	"time"

	"github.com/mongodb/mongo-go-driver/mongo"
)

var (
	//DB connection context
	ctx, _ = context.WithTimeout(context.Background(), 10*time.Second)

	//DB connection client
	client, err = mongo.Connect(ctx, "mongodb://localhost:27017")

	//Mongo data base instance
	db = client.Database("Smart-switcher")

	//Mongo collections
	BotConfig = db.Collection("BotConfig")
)
